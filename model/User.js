const Sequelize = require('sequelize')
const sequelize = require('../server')

const User = sequelize.define('user', {
  username: Sequelize.STRING,
  birthday: Sequelize.DATE
});

sequelize.sync()
.then(() => User.create({
  username: 'Vitor Ivan DAngelo',
  birthday: new Date(1980, 6, 20)
}))
.then(jane => {
  console.log(jane.get({
    plain: true
  }));
});