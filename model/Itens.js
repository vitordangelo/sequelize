const Sequelize = require('sequelize')
const sequelize = require('../server')

const Item = sequelize.define('item', {
  description: {
    type: Sequelize.STRING,
    validate: {
      len: {
        args: [10, 150],
        msg: "Teste de mensagem de erro"
      }
    }
  },
  size: Sequelize.STRING
});

sequelize.sync()
.then(() => Item.create({
  description: 'Coll',
  size: '300x300'
}))
.then(item => {
  console.log(item.get({
    plain: true
  }));
}).catch((error)=> {
  console.log(error);
})