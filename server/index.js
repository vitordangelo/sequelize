const Sequelize = require('sequelize')
const sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize', {
  host: 'localhost',
  dialect: 'mysql',
  port: 3307,

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

module.exports = sequelize
